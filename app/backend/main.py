from fastapi import FastAPI

app = FastAPI()


# Get CORS origins from an environment variable
cors_origins = os.getenv("CORS_ORIGINS", "http://localhost:8080")
origins = cors_origins.split(",")

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get("/")
async def root():
    return {"message": "Hello, World!"}
