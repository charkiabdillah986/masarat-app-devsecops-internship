variable "region" {
  description = "The AWS region where the EKS cluster should be created."
  type        = string
}

# variable "subnet_ids" {
#   description = "A list of subnet IDs to place the EKS cluster in."
#   type        = list(string)
#   default     = []
# }

variable "backend_image" {
  description = "The Docker image for the backend application."
  type        = string
}

variable "frontend_image" {
  description = "The Docker image for the frontend application."
  type        = string
}

variable "cluster_name" {
  description = "The name of the EKS cluster."
  type        = string
}
